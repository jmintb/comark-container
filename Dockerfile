FROM ubuntu:18.04
MAINTAINER Jessie Chatham Spencer <jessie@teainspace.com>

WORKDIR ijvm-install-directory

# Install arm utils and ijvm dependencies.
RUN apt-get update -y && apt-get install -y flex make byacc qemu-user-static gcc-arm-linux-gnueabi

# Download ijvm installation files.
ADD https://users-cs.au.dk/~bouvin/dComArk/2015/noter/Note_2/ijvm-tools-0.9.tar.gz .
RUN tar -xf ijvm-tools-0.9.tar.gz

# Install ijvm.
WORKDIR ijvm-tools-0.9
RUN ./configure
RUN make
RUN make install

# Cleanup
WORKDIR ..
RUN rm ijvm-tools-0.9.tar.gz

# Move to directory that will be used for mounting project files.
WORKDIR /project
